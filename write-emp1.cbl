       IDENTIFICATION DIVISION. 
       PROGRAM-ID. WRITE-EMP1. 
       AUTHOR. TITIMA.

       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           SELECT EMP-FILE ASSIGN TO  "EMP1.DAT" 
              ORGANIZATION IS LINE SEQUENTIAL.
       
       DATA DIVISION. 
       FILE SECTION.
       FD EMP-FILE.
       01 EMP-DETAILS.
           88 END-OF-EMP-FILE VALUE HIGH-VALUE.
           05 EMP-SSN PIC 9(9).
           05 EMP-NAME.
              10 EMP-SURNAME PIC X(15).
              10 EMP-FORENAME PIC X(10).
           05 EMP-DATE-OF-BIRTH.
              10 EMP-YDB PIC 9(4).
              10 EMP-MDB PIC 9(2).
              10 EMP-DDB PIC 9(2).
           05 EMP-GENDER PIC X.

       PROCEDURE DIVISION.
       BEGIN.
           OPEN OUTPUT EMP-FILE 
           MOVE "123456789" TO EMP-SSN 
           MOVE "PRATPRAYOON" TO EMP-SURNAME 
           MOVE "TITIMA" TO EMP-FORENAME
           MOVE "20001126" TO EMP-DATE-OF-BIRTH 
           MOVE "F" TO EMP-GENDER
           WRITE EMP-DETAILS

           MOVE "987654321" TO EMP-SSN 
           MOVE "WERAPAN" TO EMP-SURNAME 
           MOVE "WORAWIT" TO EMP-FORENAME
           MOVE "19780923" TO EMP-DATE-OF-BIRTH 
           MOVE "M" TO EMP-GENDER
           WRITE EMP-DETAILS

           MOVE "543216789" TO EMP-SSN 
           MOVE "MANGMEE" TO EMP-SURNAME 
           MOVE "MANEE" TO EMP-FORENAME
           MOVE "20021204" TO EMP-DATE-OF-BIRTH 
           MOVE "F" TO EMP-GENDER
           WRITE EMP-DETAILS

           CLOSE EMP-FILE
           GOBACK 
           .